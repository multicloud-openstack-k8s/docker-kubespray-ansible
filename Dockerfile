FROM python:2-alpine

RUN apk --no-cache add --virtual build-dependencies libffi-dev openssl-dev build-base
ADD https://raw.githubusercontent.com/kubernetes-incubator/kubespray/v2.6.0/requirements.txt .
RUN pip install -r requirements.txt \
  && pip install ovh \
  && apk del build-dependencies \
  && apk --no-cache add git terraform openssh-client jq bash

# Tests
RUN ansible-playbook --version \
  && terraform -version \
  && git --version \
  && jq --version
